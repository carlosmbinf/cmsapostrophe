
var path = require('path');
var passport = require('passport');
var OAuth2Strategy = require('passport-oauth2').Strategy;


var apos = require('apostrophe')({
  shortName: 'krly',

  // See lib/modules for basic project-level configuration of our modules
  // responsible for serving static assets, managing page templates and
  // configuring user accounts.
 

 // YOU MUST CONFIGURE baseUrl. For local dev testing
  // you can set it to http://localhost:3000, for production
  // it must be real  
 
baseUrl: 'https://apostrophe.dnsroute53test.net',
  
  modules: {
  //  'apostrophe-passport': {
  //     strategies: [
  //       {
  //         // gitlab login
  //         // You must npm install --save this module in your project first
  //         module: 'passport-gitlab2',
  //         options: {
  //            // Opciones para passport-gitlab2, consulte la documentación de ese módulo, 
  //           // no tiene que establecer callbackURL 
  //           clientID: '64120483a8f71701292e3517a43464ce6cd84b11b8213a0d6f1206287563f268',
  //           clientSecret: 'fbc1667493a9e2952feb44ddec8d4722423d18bd88eafeaf7bf31cb0ff1b2e87',
  //           callbackURL: "http://apostrophe.dnsroute53test.net/auth/gitlab/callback",
            
  //         }
  //       }
  //     ],
  //      create: {
  //   // Presence of "group" means we'll add them to a group...
  //   group: {
  //     // Called "google"...
  //     title: 'gitlab',
  //     // With these Apostrophe permissions (admin can do ANYTHING, so be careful)
  //     permissions: [ 'admin' ]
  //   }
  // }
  //   },

 'apostrophe-passport': {
      strategies: [
        {

          // LOGIN CON PASSPORT-OAUTH2
          // gitlab login
          // You must npm install --save this module in your project first
          // module: 'passport-oauth2',
          // options: {
          //    // Opciones para passport-gitlab2, consulte la documentación de ese módulo, 
          //   // no tiene que establecer callbackURL 
          //   tokenURL: 'https://auth.vrewardsapps.com/oauth2/token',
          //   authorizationURL: 'https://auth.vrewardsapps.com/oauth2/authorize',
          //   clientID: '1b554567-7af4-4be5-a006-0ca80235d9ac',
          //   clientSecret: '6PWiUg1sssjRfTSfuZ9RObaN1jB5m4MqYX1FiGs4HpE',
          //   callbackURL: 'http://apostrophe.dnsroute53test.net/auth/oauth2/callback',
          //   passReqToCallback: true

          // LOGIN CON PASSPORT-OAUTH
          module: 'passport-oauth2',
          options: {
          
                requestTokenURL: 'https://auth.vrewardsapps.com/request_token',
                authorizationURL: 'https://auth.vrewardsapps.com/oauth2/authorize',
                tokenURL: 'https://auth.vrewardsapps.com/oauth2/token',
                clientID: '1b554567-7af4-4be5-a006-0ca80235d9ac', 
                clientSecret: '6PWiUg1sssjRfTSfuZ9RObaN1jB5m4MqYX1FiGs4HpE',
                callbackURL: 'https://apostrophe.dnsroute53test.net/auth/oauth2/callback',
                passReqToCallback: true,
                response_type:'token'
          },
          authenticate: {
            // minimum scopes for matching logins based on email addresses.
            // profile is absolutely required, you almost certainly want email too
            scope: [ 'profile', 'email' ]
          }
        }
        

      ],
       create: {
    // Presence of "group" means we'll add them to a group...
    group: {
      // Called "google"...
      title: 'gitlab',
      // With these Apostrophe permissions (admin can do ANYTHING, so be careful)
      permissions: [ 'admin' ]
    }
  }
    },

  //   'apostrophe-passport': {
  //     strategies: [
  //     {
  //      module: 'passport-oauth2',
  //      options: {
  //       authorizationURL: 'https://auth.vrewardsapps.com/oauth2/authorize',
  //       tokenURL: 'https://auth.vrewardsapps.com/oauth2/token',
  //       clientID: '1b554567-7af4-4be5-a006-0ca80235d9ac',
  //       clientSecret: '6PWiUg1sssjRfTSfuZ9RObaN1jB5m4MqYX1FiGs4HpE'
  //       callbackURL: "http://localhost:3000/auth/oauth2/callback"
  //     }
  //   }
  //   ] 
  // },
  'apostrophe-login': {
    // We disable it here, by configuring the built-in apostrophe-login` module
    localLogin: true
  },

'apostrophe-pages': {
      types: [
        {
          name: 'default',
          label: 'Default'
        },
        {
          name: 'home',
          label: 'Home'
        },
      ]
    },
     'apostrophe-users': {
      groups: [
        {
          title: 'guest',
          permissions: [ 'guest' ]
        },
        {
          title: 'gitlab',
          permissions: [ 'admin' ]
        },
        {
          title: 'admin',
          permissions: [ 'admin' ]
        }
      ]
    },
    // Apostrophe module configuration

    // Note: most configuration occurs in the respective
    // modules' directories. See lib/apostrophe-assets/index.js for an example.
    
    // However any modules that are not present by default in Apostrophe must at
    // least have a minimal configuration here: `moduleName: {}`

    // If a template is not found somewhere else, serve it from the top-level
    // `views/` folder of the project

    'apostrophe-templates': { viewsFolderFallback: path.join(__dirname, 'views') },
    'apostrophe-db': {
    uri: 'mongodb://ec2-100-24-59-189.compute-1.amazonaws.com:5000'
  }
  }
});
